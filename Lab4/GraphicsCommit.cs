﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Troschuetz.Random;
namespace Lab4
{
    public partial class Graphics : Form
    {
        Data dt;
        public Graphics(Data _dt)
        {
            InitializeComponent();
            dt = _dt;
            
        }
        NormalDistribution nd;
        private void Graphics_Load(object sender, EventArgs e)
        {
            nd = new NormalDistribution();
            decimal test = Convert.ToDecimal(dt.Nm / dt.Nt);
            dt.disp = Math.Sqrt(Math.Pow(dt.wd, 2.0) + Math.Pow(dt.td, 2.0));
            dt.Z = 1.0 - Convert.ToDouble((dt.Nm / dt.Nt));
            dt.p = 1.0 - dt.Z;
            decimal sum = 0;
            decimal temp = 0;
            List<double> dd = new List<double>();
            test = Convert.ToDecimal(1.0 - dt.p);
            double lasti = 0;
            double mi = 0.0;
            //double mnozh = Convert.ToDouble(0.001);
            for(double i=-5.0;i<5.0;i+=0.01)
            {
                temp += Convert.ToDecimal((1.0 / Math.Sqrt(2 * Math.PI))*Math.Pow(Math.E,-(Math.Pow(i,2.0)/2)));
                temp += Convert.ToDecimal((1.0 / Math.Sqrt(2 * Math.PI)) * Math.Pow(Math.E, -(Math.Pow(i+0.01, 2.0) / 2)));
                temp /= 2;
                temp *= Convert.ToDecimal(0.01);
                
                sum += temp;
                
                temp = Convert.ToDecimal(0.0);
                if(sum>=test)
                {
                    dt.K = i;
                    break;
                }
                lasti = i;
            }
            nd.Sigma = dt.td;
            nd.Mu = dt.tm;
            dt.dt = nd.NextDouble();
            nd.Sigma = dt.wd;
            nd.Mu = dt.wm;
            dt.dw = nd.NextDouble();
            dt.b = dt.K * dt.disp;
            dt.M = dt.wm * (dt.tm + dt.ta) + dt.b;
            dt.Pk = dt.wm * (dt.tm + ((0.5) * dt.ta));
            double currentx = dt.M;
            double[] mas;
            
            double current = dt.M;
            double time=0.0;
            bool checks = false;
            for (double i = 1; i < dt.endpoint; i ++)
            {
                mas = new double[1];
                mas[0] = dt.M;
                chart1.Series["M"].Points.AddXY(i, mas.Cast<object>().ToArray());
                mas[0] = dt.Pk;
                chart1.Series["P"].Points.AddXY(i, mas.Cast<object>().ToArray());
                mas[0] = dt.b;
                chart1.Series["B"].Points.AddXY(i, mas.Cast<object>().ToArray());
            }
            NormalDistribution ndd = new NormalDistribution();
            ndd.Sigma = dt.td;
            ndd.Mu = dt.tm;
            double count2 = 0.0;
            double lastj = 0.0;
            bool getinf = false;
            chart1.Series["Wt"].Points.AddXY(0.0, current);
            for (double i=1+dt.dt;i<dt.endpoint;i++)
            {
                mas = new double[1];
                if(count2>= dt.ta)
                {
                    if(current<=dt.Pk)
                    {
                        dt.tdost = ndd.NextDouble();
                        checks = true;
                    }
                    count2 = 0.0;
                }
                if(checks==true)
                {
                    mi = (dt.M-current)+(dt.wm)*(dt.tm);
                    for(double j=i;j<i+dt.tdost;j+=dt.dt)
                    {
                        dt.dw = nd.NextDouble();
                        current -= dt.dw;
                        chart1.Series["Wt"].Points.AddXY(j, current);
                        
                        lastj = j;
                        count2 += j;
                        getinf = true;
                    }
                    current += mi;
                    if (getinf == false)
                    {
                        lastj = i + dt.dt;
                    }
                     
                    else
                    {
                        getinf = false;
                        //lastj += dt.dt;
                    }
                    chart1.Series["Wt"].Points.AddXY(lastj, current);
                    checks = false;
                    
                    i = lastj;
                    continue;

                }
                dt.dw = nd.NextDouble();
                current -= dt.dw;
                chart1.Series["Wt"].Points.AddXY(i, current);
                
                count2 += i;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.SaveImage("graphic.jpg",System.Windows.Forms.DataVisualization.Charting.ChartImageFormat.Jpeg);
        }
    }
}
