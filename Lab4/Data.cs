﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Troschuetz.Random;
namespace Lab4
{
    [DataContract]
    public class Data
    {
        [DataMember]
        public double wm { get; set; }
        [DataMember]
        public double wd { get; set; }
        [DataMember]
        public double tm { get; set; }
        [DataMember]
        public double td { get; set; }
        [DataMember]
        public double ta { get; set; }
        [DataMember]
        public double Nt {get;set;}
        [DataMember]
        public double Nm { get; set; }
        [DataMember]
        public double endpoint { get; set; }
        public double K { get; set; }
        public double Z { get; set; }
        public double Pk { get; set; }
        public double tdost { get; set; }
        public double zak { get; set; }
        public double z { get; set; }
        public double p { get; set; }
        public double b { get; set; }
        public double disp { get; set; }
        public double dt { get; set; }
        public double dw { get; set; }
        public double M { get; set; }
    }
}
