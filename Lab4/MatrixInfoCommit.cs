﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication25
{
    [DataContract]
    class MatrixInfo
    {
        [DataMember]
        public bool Three { get; set; }
        [DataMember]
        public int[][] Matrix { get; set; }
        [DataMember]
        public string[] MachineName { get; set; }
        [DataMember]
        public string Parts_name { get; set; }
       
    }
}
