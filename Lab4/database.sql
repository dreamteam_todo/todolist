/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     11.05.2017 21:42:08                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Liked') and o.name = 'FK_LIKED_LIKED_USER')
alter table Liked
   drop constraint FK_LIKED_LIKED_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Liked') and o.name = 'FK_LIKED_LIKED2_PLACE')
alter table Liked
   drop constraint FK_LIKED_LIKED2_PLACE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_ABOUT_PLACE')
alter table Review
   drop constraint FK_REVIEW_ABOUT_PLACE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_WRITES_USER')
alter table Review
   drop constraint FK_REVIEW_WRITES_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"User"') and o.name = 'FK_USER_WHICH_SEC_SECRET_Q')
alter table "User"
   drop constraint FK_USER_WHICH_SEC_SECRET_Q
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Liked')
            and   name  = 'Liked_FK'
            and   indid > 0
            and   indid < 255)
   drop index Liked.Liked_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Liked')
            and   name  = 'Liked2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Liked.Liked2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Liked')
            and   type = 'U')
   drop table Liked
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Place')
            and   type = 'U')
   drop table Place
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'writes_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.writes_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'about_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.about_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Review')
            and   type = 'U')
   drop table Review
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"User"')
            and   name  = 'Which_secret_question_FK'
            and   indid > 0
            and   indid < 255)
   drop index "User".Which_secret_question_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"User"')
            and   type = 'U')
   drop table "User"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('secret_questions')
            and   type = 'U')
   drop table secret_questions
go

/*==============================================================*/
/* Table: Liked                                                 */
/*==============================================================*/
create table Liked (
   id_user              int                  not null,
   id_place             int                  not null,
   constraint PK_LIKED primary key nonclustered (id_user, id_place)
)
go

/*==============================================================*/
/* Index: Liked2_FK                                             */
/*==============================================================*/
create index Liked2_FK on Liked (
id_place ASC
)
go

/*==============================================================*/
/* Index: Liked_FK                                              */
/*==============================================================*/
create index Liked_FK on Liked (
id_user ASC
)
go

/*==============================================================*/
/* Table: Place                                                 */
/*==============================================================*/
create table Place (
   id_place             int                  not null,
   name_place           varchar(128)         not null,
   address_place        varchar(128)         not null,
   f_place              FLOAT10              not null,
   l_place              FLOAT10              not null,
   constraint PK_PLACE primary key nonclustered (id_place)
)
go

/*==============================================================*/
/* Table: Review                                                */
/*==============================================================*/
create table Review (
   id_review            int                  not null,
   id_place             int                  not null,
   id_user              int                  not null,
   text_review          varchar(2048)        not null,
   rating_review        float(7)             not null,
   date_review          datetime             not null,
   constraint PK_REVIEW primary key nonclustered (id_review)
)
go

/*==============================================================*/
/* Index: about_FK                                              */
/*==============================================================*/
create index about_FK on Review (
id_place ASC
)
go

/*==============================================================*/
/* Index: writes_FK                                             */
/*==============================================================*/
create index writes_FK on Review (
id_user ASC
)
go

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" (
   id_user              int                  not null,
   id_secret_question   int                  not null,
   "e-mail_user"        varchar(64)          not null,
   password_user        varchar(64)          not null,
   secret_question_answer_user varchar(64)          not null,
   constraint PK_USER primary key nonclustered (id_user)
)
go

/*==============================================================*/
/* Index: Which_secret_question_FK                              */
/*==============================================================*/
create index Which_secret_question_FK on "User" (
id_secret_question ASC
)
go

/*==============================================================*/
/* Table: secret_questions                                      */
/*==============================================================*/
create table secret_questions (
   id_secret_question   int                  not null,
   secret_question      varchar(128)         not null,
   constraint PK_SECRET_QUESTIONS primary key nonclustered (id_secret_question)
)
go

alter table Liked
   add constraint FK_LIKED_LIKED_USER foreign key (id_user)
      references "User" (id_user)
go

alter table Liked
   add constraint FK_LIKED_LIKED2_PLACE foreign key (id_place)
      references Place (id_place)
go

alter table Review
   add constraint FK_REVIEW_ABOUT_PLACE foreign key (id_place)
      references Place (id_place)
go

alter table Review
   add constraint FK_REVIEW_WRITES_USER foreign key (id_user)
      references "User" (id_user)
go

alter table "User"
   add constraint FK_USER_WHICH_SEC_SECRET_Q foreign key (id_secret_question)
      references secret_questions (id_secret_question)
go

