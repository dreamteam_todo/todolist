﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using Troschuetz.Random;
using System.IO;

namespace Lab4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NormalDistribution nd = new NormalDistribution();
            nd.Mu = 5;
            nd.Sigma = 10;

            
        }
        Data sr;
        bool fl = false;
        private void button1_Click(object sender, EventArgs e)
        {
            DataContractJsonSerializer srr = new DataContractJsonSerializer(typeof(Data));
            FileStream fs = new FileStream("userfile.txt",FileMode.Open);
            sr = (Data)srr.ReadObject(fs);
            textBox1.Text = sr.wm.ToString();
            textBox2.Text = sr.wd.ToString();
            textBox3.Text = sr.tm.ToString();
            textBox4.Text = sr.td.ToString();
            textBox5.Text = sr.ta.ToString();
            textBox6.Text = sr.Nt.ToString();
            textBox7.Text = sr.Nm.ToString();
            textBox8.Text = sr.endpoint.ToString();
            fl = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(fl==false)
            {
                sr = new Data();
            }
            sr.wm = Convert.ToDouble(textBox1.Text);
            sr.wd = Convert.ToDouble(textBox2.Text);

            sr.tm = Convert.ToDouble(textBox3.Text);
            sr.td = Convert.ToDouble(textBox4.Text);
            sr.ta = Convert.ToDouble(textBox5.Text);
            sr.Nt = Convert.ToDouble(textBox6.Text);
            sr.Nm = Convert.ToDouble(textBox7.Text);
            sr.endpoint = Convert.ToDouble(textBox8.Text);
            Graphics gr = new Graphics(sr);
            gr.ShowDialog();
        }
    }
}
